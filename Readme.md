# Self-Hosting GitLab in Docker

## Gitlab
1. Run gitlab server in docker container
    ```
    docker-compose up -d
    ```

2. Change root user password
    ```
    doceker exec -it gitlab bash

    gitlab-rails console -e production 
    user = User.where(id: 1).first
    user.password = 'secret_pass'
    user.password_confirmation = 'secret_pass' 

    user.save!
    ```

## Gitlab Runner
1. Install the Docker image and start the container
    ```
    docker run -d --name gitlab-runner --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
    ```
2. Register the runner
   ```
   docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
   ```
   * [Gitlab Executors](https://docs.gitlab.com/runner/executors/)

## .gitlab-ci.yml
1. [documents](https://docs.gitlab.com/ee/ci/yaml/)
2. gitlab-ci auto build docker image and push to dockerhub ([link](https://ithelp.ithome.com.tw/articles/10266722))